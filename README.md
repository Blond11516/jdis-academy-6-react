## Voici les features à implémenter:
- L'utilisateur doit pouvoir choisir s'il veut afficher la liste des noms de famille, la liste des prénoms féminins et/ou la liste des prénoms masculins.
- L'utilisateur peut sélectionner plus qu'une liste à afficher en même temps.
- Selon les listes sélectionnées, un tableau doit afficher les noms avec ces colonnes:
    - Type de nom (Last name, first name male, first name female)
	- Id
	- name
	- weight
- Si aucune liste n'est sélectionnée, le tableau ne s'affiche pas.
- Le tableau permet à l'utilisateur de trier la liste selon chacune des colonnes en cliquant sur le nom de la colonne.
    - En recliquant sur la colonne, le tri est inversé (décroissant au lieu de croissant)
    - Un indicateur visuel permet de voir quelle colonne est utilisé pour le tri et quelle direction (croissante/décroissante) est utilisée
- Il est interdit d'utiliser une librairie/package autre que le framework front-end utilisé pour faire le tableau.
- Un bouton permet de générer un nom complet au hasard à partir des listes sélectionnées. Le format d'affichage du nom complet est laissé à votre discrétion.
    - Le bouton ne s'affiche pas si les listes sélectionnées ne permettent pas de faire un nom complet (ex: juste last name sélectionné)
	- La génération du nom complet utilise le weight de chaque nom (donc, si on a deux noms de familles, un avec un poids de 1 et l'autre un poids de 2, celui avec un poids de 2 a 2/3 chance d'être le nom sélectionné).

Note: il est FORTEMENT recommandé d'utiliser un framework front-end (ex: Angular, React, VueJS) pour faire ce challenge.

**La correction est manuelle. Vous devez montrer votre travail à Émilio Gonzalez pour obtenir les points.**

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `npm run build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
