async function get(url) {
    let response = await fetch(url);
    return response.json();
}

class API {
    async getMaleFirstNames() {
        return await get('http://159.89.118.144:3001/names/first/male');
    }

    async getFemaleFirstNames() {
        return await get('http://159.89.118.144:3001/names/first/female');
    }

    async getLastNames() {
        return await get('http://159.89.118.144:3001/names/last');
    }
}

export default new API();