import React, { Component } from 'react';
import './App.css';
import API from './api'

class App extends Component {
  constructor() {
    super()
    this.state = {};
  }
  
  async componentDidMount() {
    let names = await API.getLastNames();
    this.setState({names: names.names})
  }
  
  render() {
    return (
      <div className="App">
        {this.state.names &&
          this.state.names.slice(0, 10).map(name => <p key={name.id} >{name.name}</p>)
        }
      </div>
    );
  }
}

export default App;
